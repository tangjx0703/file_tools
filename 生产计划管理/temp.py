import pandas as pd
import numpy as np
fpath = r'/Users/tangjx/Documents/python/file_tools/生产计划管理/周计划与工作执行情况核查表.xlsx'
df_w = pd.read_excel(fpath, sheet_name='周计划', keep_default_na=True)
df_m = pd.read_excel(fpath, sheet_name='月计划', keep_default_na=True)
df1 = pd.merge(df_m, df_w, on = '周计划编号', how = 'left')
df2 = pd.merge(df_w, df_m, on = '周计划编号', how='left')

print(df1.head(5))
print(df2.head(5))

# df1.to_excel('月计划执行情况.xlsx')
# df2.to_excel('周计划执行情况.xlsx')
print(pd.pivot_table(df_m, index=['工作地点'], values=['设备名称'], aggfunc='count'))
      
      


