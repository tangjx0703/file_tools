import pandas as pd
import numpy as np
import os
import plan_op
import dbinfo
import sql_op

if __name__ == '__main__':
    Tuser, Tpassword, Thost, Tdatabase, Tport = dbinfo.dbinfo()
    print(Tdatabase)
    pdsqlio = sql_op.pd_sql(Tuser, Thost, Tport, Tpassword, Tdatabase)
    f_plan = plan_op.planop()
    plan_tables=['月计划表','周计划表','工作执行表']
    
    selected = """----------------------------------
    1、导入月计划
    2、导入周计划
    3、导入生产信息日报
    0、退出程序
    """
    print(selected)
    while (True):
        flag = int(input('请选择需要操作的序号：'))
        if (flag < 1 and flag > 2):
            print('输入错误！')
            flag = int(input('请选择需要操作的序号：'))
        else:
            if flag == 1:
                
                f_path = input('请输入月计划工作簿路径：')
                sh_name = input('请输入月计划表格名称：')
                df = pd.DataFrame()
                df = f_plan.m_plan_input(f_path, sh_name)
                pdsqlio.save_data(plan_tables[0],df)
            elif flag == 2:
                f_path = input('请输入周计划工作簿路径：')
                sh_name = input('请输入周计划表格名称：')
                df = pd.DataFrame()
                df = f_plan.w_plan_input(f_path, sh_name)
                pdsqlio.save_data(plan_tables[1], df)
            elif flag == 3:
                dir_path = input('请输入生产信息日报文件夹路径：')
                df = pd.DataFrame()
                df = f_plan.daily_input(dir_path)
                pdsqlio.save_data(plan_tables[2], df)
            elif flag == 0:
                print('结束！')
                break
            else:
                print('输入错误！请重新选择')
    pdsqlio.disconnect()
    
    
    
    # # f_path = r'/Users/tangjx/Documents/python/file_tools/生产计划管理/月度计划/宁波电网2021年3月份停电计划（流程稿）.xlsx'
    # # sh_name = '220kV停电计划'
    # f_path = r'/Users/tangjx/Documents/python/file_tools/生产计划管理/周计划/周计划 2021年3月1日-3月7日.xlsx'
    # sh_name = 'Sheet1'
    # f_plan = plan_op.planop()
    # df = pd.DataFrame()
    # df = f_plan.w_plan_input(f_path, sh_name)
    # print(df.head(5))
    # df.to_excel('周计划3月1-7.xlsx')
    # # df = f_plan.m_plan_input(f_path, sh_name)
    # # df.to_excel('月计划.xlsx')
    # # print(df.head(2))
    # # print(df.info())
    # # f_path.m_plan_input(f_path, sh_name)
