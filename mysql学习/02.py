import pymysql

# 建立数据库连接
conn = pymysql.connect(
    host='localhost',
    port=3306,
    user='root',
    password='tjx890612',
    db='机器人巡检数据分析',
    charset='utf8'
)

# 获取游标
cursor = conn.cursor()

# 执行sql语句
sql = 'ALTER TABLE 操作票统计 MODIFY COLUMN 发令时间 datetime'
rows = cursor.execute(sql)

# 提交
conn.commit()

# 关闭游标
cursor.close()

# 关闭连接
conn.close()
